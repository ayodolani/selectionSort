﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace selectionSort
{
    class Program
    {
        static void Main(string[] args)
        {
            string s = Console.ReadLine();
            int[] ar = s.Split(',').Select(str => int.Parse(str)).ToArray();
         
            int n = ar.Length;
            for (int x = 0; x < n; x++)
            {
                int first = x;
                for (int y = x; y < n; y++)
                {
                    if (ar[first] > ar[y])
                    {
                        first = y;
                    }
                }
                int temp = ar[x];
                ar[x] = ar[first];
                ar[first] = temp;
            }

            Console.WriteLine("The Sorted Array : ");

            foreach (int i in ar)
            {
                Console.Write(i + " ");

            } Console.ReadKey();
        }
    }
}